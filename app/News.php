<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class News extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'news';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'content',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
