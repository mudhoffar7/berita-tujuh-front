<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['top_news'] = News::orderBy('created_at', 'desc')->first();
        $data['side_news'] = News::orderBy('created_at', 'asc')->get()->take(2);
        $data['list_news'] = News::get();
        return view('home', $data);
    }

    public function show($id)
    {
        $data['news'] = News::find($id);
        return view('show', $data);
    }
}
