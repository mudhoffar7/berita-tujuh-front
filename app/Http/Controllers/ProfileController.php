<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class ProfileController extends Controller
{
    public function updateProfile(Request $request)
    {
    	$data = $request->all();
    	$profile = Profile::where('user_id', $data['user_id'])->first();
    	$profile->update($data, ['except'=>'_token']);
    	
    	return redirect('/news');
    }
}
