<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Redirect;

use App\User;
use App\Profile;

class AuthController extends Controller
{
    public function showLoginForm() 
    {
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        if ( Auth::attempt(['email' => $request->email, 'password' => $request->password]) ) {
            return redirect('/news');
        } else {
            return redirect('/login')->with('message', 'Email atau Password salah');
        }
    }

    public function showRegisterForm()
    {
        return view('auth.register');
    }

    public function doRegister(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        } else {
            $data = $request->all();
            $data['password'] = bcrypt($data['password']);
            
            $user = User::Create($data);
            $profile['user_id'] = $user->id;
            Profile::create($profile);

            if ( Auth::attempt(['email' => $request->email, 'password' => $request->password]) ) {
                return redirect('/news');
            }
        }
    }

    public function doLogout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
