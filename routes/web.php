<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/show/{newsid}', 'HomeController@show');
//login
Route::get('/login', 'AuthController@showLoginForm')->name('login');
Route::post('/login', 'AuthController@doLogin');
//register
Route::get('/register', 'AuthController@showRegisterForm');
Route::post('/register', 'AuthController@doRegister');
//logout

Route::middleware(['auth'])->group(function(){
	Route::post('/logout', 'AuthController@doLogout');
	//update profile
	Route::post('/update-profile', 'ProfileController@updateProfile');
	//resource news
	Route::resource('news', 'NewsController');

});