@extends('layouts.author.app')

@section('title','News')

@section('sidebar')
    @include('components.admin.sidebar')
@endsection

@section('custom-css')
	<link href="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/adminbsb-materialdesign/plugins/animate-css/animate.css')}}" rel="stylesheet" />
    <link href="{{url('bower_components/adminbsb-materialdesign/plugins/light-gallery/css/lightgallery.css')}}" rel="stylesheet">

@endsection
@section('content')
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Create New News
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <form action="{{ url('news')}}" method="post">
                        {{ csrf_field()}}
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <label>Title :</label>
                        <input type="text" class="form-control" name="title">
                        <label>Content :</label>
                        <textarea id="ckeditor" name="content"></textarea>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success pull-right">SAVE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom-js')
<script>
    $(window).load(function(){
        $("#newsTable").dataTable();
    })
</script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/light-gallery/js/lightgallery-all.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/js/pages/medias/image-gallery.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/js/pages/tables/jquery-datatable.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/js/pages/forms/editors.js')}}"></script>
@endsection