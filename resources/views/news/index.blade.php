@extends('layouts.author.app')

@section('title','News')

@section('sidebar')
    @include('components.admin.sidebar')
@endsection

@section('custom-css')
	<link href="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/adminbsb-materialdesign/plugins/animate-css/animate.css')}}" rel="stylesheet" />
    <link href="{{url('bower_components/adminbsb-materialdesign/plugins/light-gallery/css/lightgallery.css')}}" rel="stylesheet">
@endsection
@section('content')
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-2">
                            <h2>
                            DATA NEWS
                            </h2>    
                        </div>
                        <div class="col-md-10">
                            <a class="btn btn-warning pull-right" href="{{ url('/news/create')}}">Create New</a>
                        </div>
                    </div>  
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="newsTable" class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Create Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $item)
                               <tr>
                                   <td>{{ $item->title}}</td>
                                   <td>{!!$item->content!!}</td>
                                   <td>{{ $item->created_at}}</td>
                                   <td>
                                        <form id="delete-{{$item->id}}" action="{{url('news', $item->id)}}" method="POST">
                                            {{ csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                        <a class="btn btn-primary" href="{{ route('news.edit', $item->id)}}">Edit</a>
                                        <button class="btn btn-danger" onclick="deleteNews({{$item->id}})">Delete</button>
                                    </td>
                               </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
<script>
    $(window).load(function(){
        $("#newsTable").dataTable();
    })
</script>
<script type="text/javascript">
    deleteNews = function(id)
    {
        if(confirm("Are you sure to delete this news?")){
            $('#delete-'+id).submit();
        }
    }
</script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/light-gallery/js/lightgallery-all.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/js/pages/medias/image-gallery.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script src="{{url('bower_components/adminbsb-materialdesign/js/pages/tables/jquery-datatable.js')}}"></script>
@endsection