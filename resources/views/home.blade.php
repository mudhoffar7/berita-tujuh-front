@extends('layouts.base')

@section('content')
<div class="featured-post-area">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-8">
                <div class="row">
                    <!-- Single Featured Post -->
                    <div class="col-12 col-lg-7">
                        <div class="single-blog-post featured-post">
                            <div class="post-thumb">
                                <a href="#"><img src="{{asset('newspaper/img/bg-img/16.jpg')}}" alt=""></a>
                            </div>
                            <div class="post-data">
                                <a href="{{ url('show', $top_news->id)}}" class="post-title">
                                    <h6>{{ $top_news->title}}</h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">By <a href="#">{{ $top_news->user->name }}</a></p>
                                    <p class="post-excerp">{!! $top_news->content!!}</p>
                                    <!-- Post Like & Post Comment -->
                                    <div class="d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="{{asset('newspaper/img/core-img/like.png')}}" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="{{asset('newspaper/img/core-img/chat.png')}}" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-5">
                        @foreach($side_news as $item)
                        <!-- Single Featured Post -->
                        <div class="single-blog-post featured-post-2">
                            <div class="post-thumb">
                                <a href="#"><img src="{{asset('newspaper/img/bg-img/17.jpg')}}" alt=""></a>
                            </div>
                            <div class="post-data">
                                <div class="post-meta">
                                    <a href="{{ url('show', $item->id)}}" class="post-title">
                                        <h6>{{ $item->title}}</h6>
                                    </a>
                                    <!-- Post Like & Post Comment -->
                                    <div class="d-flex align-items-center">
                                        <a href="#" class="post-like"><img src="{{asset('newspaper/img/core-img/like.png')}}" alt=""> <span>392</span></a>
                                        <a href="#" class="post-comment"><img src="{{asset('newspaper/img/core-img/chat.png')}}" alt=""> <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <!-- Single Featured Post -->
                @foreach($list_news as $item)
                <div class="single-blog-post small-featured-post d-flex">
                    <div class="post-thumb">
                        <a href="#"><img src="{{asset('newspaper/img/bg-img/19.jpg')}}" alt=""></a>
                    </div>
                    <div class="post-data">
                        <div class="post-meta">
                            <a href="{{ url('show', $item->id)}}" class="post-title">
                                <h6>{{ $item->title }}</h6>
                            </a>
                            <p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- Single Featured Post -->
            
            </div>
        </div>
    </div>
</div>
@endsection
