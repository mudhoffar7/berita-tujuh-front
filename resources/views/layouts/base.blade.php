<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
    @yield('custom-css')
</head>

<body>
    <!-- ##### Header Area Start ##### -->
    @include('layouts.navbar')
    <!-- ##### Header Area End ##### -->

    <!-- ##### Featured Post Area Start ##### -->
    @yield('content')
    <!-- ##### Featured Post Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    @include('layouts.footer')
    @yield('custom-js')
</body>

</html>