<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="active">
            <a href="{{url('/')}}">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        <li class="header">AUTHOR</li>
        <li>
            <a href="{{ url('/news')}}">
                <i class="material-icons">assignment_ind</i>
                <span>Manage News</span>
            </a>
        </li>
    </ul>
</div>
<!-- #Menu -->